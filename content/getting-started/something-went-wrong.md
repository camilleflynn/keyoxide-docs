+++
title = "Something went wrong"

weight = 4
+++

Things sometimes go wrong. Here's a list of things to think of to prevent things from going awry or fix them when it does.

## Keyoxide clients

### Error: "No public keys could be fetched using HKP"

<div class="table-container table-container--errors">

Possible issue | Solution | More information
:--------------|:---------|:----------------
The key was uploaded to [keys.openpgp.org](https://keys.openpgp.org) but the email address was not verified | Do the first upload to [keys.openpgp.org](https://keys.openpgp.org) via the web interface and confirm the email address via the link in the automatic email | [keyoxide-web#111](https://codeberg.org/keyoxide/keyoxide-web/issues/111)

</div>

### Error: "No public keys could be fetched using WKD"

<div class="table-container table-container--errors">

Possible issue | Solution | More information
:--------------|:---------|:----------------
The WKD server returns an armored key instead of a binary key | Make sure the WKD server returns a binary key | [keyoxide-web#116](https://codeberg.org/keyoxide/keyoxide-web/issues/116)
Cloudflare prevents Keyoxide from fetching keys using WKD | Disable Cloudflare's "Bot Fight Mode" | [keyoxide-web#135](https://codeberg.org/keyoxide/keyoxide-web/issues/135)
CORS prevents Keyoxide from fetching keys using WKD | Enable CORS on the Web Key Directory server | [enable-cors.org](https://enable-cors.org/)

</div>