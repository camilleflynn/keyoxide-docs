+++
title = "Overview"

weight = 0
+++

## Creating a Keyoxide profile

Creating a Keyoxide profile means creating a "digital passport", a secure file that contains your [identity claims](/understanding-keyoxide/identity-claims-proofs). An identity claim is a little piece of text that says "I claim to be user X on platform Y", for example "I claim to be @keyoxide@fosstodon.org on the fediverse". People can then verify these claims for themselves.

<p class="info">It is important to note that these "digital passport" files are NOT stored on Keyoxide servers, unlike traditional web platforms. You store your data where you feel comfortable and Keyoxide will just fetch it when it needs it.</p>

There are currently two ways to create a Keyoxide profile: [signature profiles](/signature-profiles) and [OpenPGP profiles](/openpgp-profiles).

<p class="warning">With time, signature profiles will become the easier method to use but are currently highly experimental and require some technical skills. OpenPGP profiles are currently stable to use but require advanced technical skills.</p>

## Verifying a Keyoxide profile

The reason to create a Keyoxide profile is to let others verify it, meaning they will check for themselves that the person that has created the Keyoxide profile actually controls the online accounts they claim to control.

Here is a guide on [verifying Keyoxide profiles](/getting-started/verifying-profiles). While it's possible to do most profile verifications manually, Keyoxide can help you with that task with their [Keyoxide clients](/getting-started/clients)