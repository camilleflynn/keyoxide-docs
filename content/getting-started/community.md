+++
title = "Keyoxide community"
aliases = ["/community"]

weight = 5
+++

Everyone can join the Keyoxide community and contribute in their own way, or just follow along for updates on the project.

There is a [community forum](https://community.keyoxide.org/) for in-depth discussions to shape the future of the Keyoxide project.

There is the **#keyoxide** IRC lobby on [libera.chat](https://libera.chat/) and the [#keyoxide:matrix.org](https://matrix.to/#/#keyoxide:matrix.org) Matrix room for casual daily discussions. Both of these are linked together so you can simply join one of them to follow the whole conversation.

There is also a [blog](https://blog.keyoxide.org/) with atom/RSS feeds to follow the progress of the Keyoxide project.

The source code is hosted on [codeberg.org/keyoxide](https://codeberg.org/keyoxide) where one can raise issues directly related to the code. All Pull Requests are welcome!