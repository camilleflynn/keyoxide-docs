+++
title = "Self-hosting Keyoxide"

weight = 2

[extra]
toc = true
+++

Self-hosting is an important aspect to the Keyoxide project. Users need to trust the Keyoxide instance they're using to reliably verify identities. Making Keyoxide itself decentralized means no one needs to trust a central server. If a friend or family member is hosting a Keyoxide instance, it becomes much easier to trust the instance!

Hosting Keyoxide alone will result in some claim verifications failing as they cannot be performed in the browser, such as the DNS check. This is why you would perhaps want to host a doip-proxy instance as well. The instructions are below.

## Using docker

Install docker and run:

```
docker run -d -p 3000:3000 keyoxide/keyoxide:latest
```

To run the doip-proxy, run:

```
docker run -d -p 3001:3000 keyoxide/doip-proxy:latest
```

## Using docker-compose

```
keyoxide:
    image: keyoxide/keyoxide:latest
    restart: always
    ports:
        - 3000:3000
    environment:
        - PROXY_HOSTNAME=localhost:3001
        - KX_HIGHLIGHTS_1_NAME=Your name
        - KX_HIGHLIGHTS_1_DESCRIPTION=Admin of this instance
        - KX_HIGHLIGHTS_1_FINGERPRINT=123123

doip_proxy:
    image: keyoxide/doip-proxy:latest
    restart: always
    ports:
        - 3001:3000
    environment:
        - TWITTER_BEARER_TOKEN=AAAA…
```

## Using node

```bash
git clone https://codeberg.org/keyoxide/keyoxide-web
cd keyoxide-web
yarn # or npm install
yarn run start # or npm run start
```

## Configuration

You can configure Keyoxide and the doip-proxy by adding environment variables.

Have a look at the [template.env](https://codeberg.org/keyoxide/keyoxide-web/src/branch/main/template.env) file for the different configuration settings.

### keyoxide-web

Environment variable | Type     | Required | Description
:--------------------|:---------|:---------|:-----------
`DOMAIN`             | `string` | false    | The domain on which the instance is hosted
`ONION_URL`          | `string` | false    | The onion URL that points to the same instance
`PORT`               | `int`    | false    | The port of the file server (default: 3000)
`PROXY_HOSTNAME`     | `string` | false    | The hostname of the doip-proxy instance to use
`KX_HIGHLIGHTS_1_NAME` | `string` | false    | The name of the first profile to hightlight on the main page
`KX_HIGHLIGHTS_1_DESCRIPTION` | `string` | false  | The subtitle of the first profile to hightlight on the main page
`KX_HIGHLIGHTS_1_FINGERPRINT` | `string` | false  | The fingerprint of the first profile to hightlight on the main page
`KX_HIGHLIGHTS_2_NAME` | `string` | false    | The name of the second profile to hightlight on the main page
`KX_HIGHLIGHTS_2_DESCRIPTION` | `string` | false  | The subtitle of the second profile to hightlight on the main page
`KX_HIGHLIGHTS_2_FINGERPRINT` | `string` | false  | The fingerprint of the second profile to hightlight on the main page
`KX_HIGHLIGHTS_3_NAME` | `string` | false    | The name of the third profile to hightlight on the main page
`KX_HIGHLIGHTS_3_DESCRIPTION` | `string` | false  | The subtitle of the third profile to hightlight on the main page
`KX_HIGHLIGHTS_3_FINGERPRINT` | `string` | false  | The fingerprint of the third profile to hightlight on the main page

### doip-proxy

Environment variable   | Type     | Required | Description
:----------------------|:---------|:---------|:-----------
`IRC_NICK`             | `string` | false    | The nick of your IRC verifier account
`MATRIX_ACCESS_TOKEN`  | `string` | false    | The access token of your Matrix verifier account
`MATRIX_INSTANCE`      | `string` | false    | The instance of your Matrix verifier account
`MATRIX_ROOM_ID`       | `string` | false    | Must be `!dBfQZxCoGVmSTujfiv:matrix.org`
`TWITTER_BEARER_TOKEN` | `string` | false    | The bearer token for the Twitter API
`XMPP_SERVICE`         | `string` | false    | The hostname of your XMPP verifier account
`XMPP_USERNAME`        | `string` | false    | The username of your XMPP verifier account
`XMPP_PASSWORD`        | `string` | false    | The password of your XMPP verifier account


<p class="info">All of these settings are optional but you will need to provide all the settings for each service (matrix, twitter, etc) for that verification to work.</p>

<p class="info">For Matrix verification to work, your verifier account must be a member of the <a href="https://matrix.to/#/#doipver:matrix.org">#doipver:matrix.org room</a>.</p>
