+++
title = "For service providers"

weight = 3
+++

If you have either:

* a website that allows users to create accounts,
* a messaging platform,
* any other type of service that may require users to prove their online identity.

Then you may be interested in supporting decentralized identity proofs as they allow your users to securely prove their hold over their account and strengthen their decentralized identity. All the service you host needs to do is:

* provide some form of public API which could be as simple as `/api/users/USERNAME` or `/users/USERNAME.json`,
* have this endpoint provide basic user details including their bio or ideally cryptographic fingerprints,
* make sure the endpoint is accessible across domains (`Access-Control-Allow-Origin: *`).

The [documentation](https://github.com/wiktor-k/openpgp-proofs#for-service-providers) on what is precisely required is provided by the original creator of decentralized OpenPGP identity proofs.