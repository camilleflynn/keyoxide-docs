+++
title = "Introduction"
+++

Keyoxide is a privacy-friendly tool to create and verify decentralized online identities.

Just like passports for real life identities, Keyoxide can be used to verify the online identity of people to make sure one is interacting with whom they are supposed to be and not imposters. Unlike real life passports, Keyoxide works with online identities or "personas", meaning these identities can be anonymous and one can have multiple separate personas to protect their privacy, both online and in real life.

To better understand how Keyoxide works and why it does what it does before getting started, have a look at the [Understanding Keyoxide](@/understanding-keyoxide/keyoxide.md) section and the [Frequently Asked Questions](@/getting-started/faq.md).

If you want to go ahead and create a new Keyoxide profile, go to the [Getting started](@/getting-started/_index.md) section.

## About the Keyoxide project

The Keyoxide project is fully open source with the repositories licensed to either Apache or AGPL.

Our git forge of choice is [Codeberg.org](https://codeberg.org).

[{{ get_it_on_codeberg() }}](https://codeberg.org/keyoxide)

The Keyoxide project is entirely funded by donations from individuals and groups, notably through [Liberapay](https://liberapay.com/keyoxide).

Much of the work has been funded by the [NLnet foundation](https://nlnet.nl/) and [NGI0](https://www.ngi.eu/) for which the project is very grateful.

[NLnet > Keyoxide](https://nlnet.nl/project/Keyoxide/)  
[NLnet > Keyoxide Private Key Operations](https://nlnet.nl/project/Keyoxide-PKO/)  
[NLnet > Keyoxide v2](https://nlnet.nl/project/Keyoxide-signatures/)  

Everyone can join the [Keyoxide community](/community) and contribute in their own way, or just follow along for updates on the project.

## About the Keyoxide documentation

Static site generated using [zola](https://getzola.org). Flags from [flagpack](https://flagpack.xyz).

Would you like to help us out with translations? Have a look at the [source code](https://codeberg.org/keyoxide/keyoxide-docs) or [reach out](/community) to get started!