+++
title = "Overview"
aliases = ["/signature-profiles"]

weight = 0
+++

<p class="warning">Signature profiles are an experimental feature and may be subject to changes!</p>

Signature profiles are simple text files that are signed by a cryptographic key. The contents of the text file can be read by both humans and machines, so that tools like Keyoxide can present them in a clear manner and assist with the verification process.

## Create and manage a signature profile

There are multiple ways to maintain a signature profile:

- [using GnuPG](/signature-profiles/using-gnupg)
- [using Kleopatra](/signature-profiles/using-kleopatra)

## Distribute the signature profile

Once you have created a signature profile, you need to make sure others can fetch it to verify it. Have a look at the [Distributing a signature profile](/signature-profiles/distributing) guide.