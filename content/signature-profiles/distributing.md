+++
title = "Distributing a profile"

weight = 3
+++

<p class="warning">Signature profiles are an experimental feature and may be subject to changes!</p>

## The direct method

Since a signature profile is just a piece of text, you could manually send it to people, embed it on your website or post it on a forum. Just copy-and-paste and you have full control over where the signature profile is available.

The drawbacks of this method is that people need to go looking manually for your signature profile — which may be an advantage for some!

<p class="info">Currently, this is the only method of distributing signature profiles. We are working on additional methods. Feel free to join the <a href="/community">community</a> to help shape these new methods, or follow the progress.</p>