+++
title = "Pixelfed"

[extra]
claim_syntax = "https://DOMAIN/users/USERNAME"
claim_variables = ["DOMAIN", "USERNAME"]
+++

Log in to your Pixelfed instance and add the proof to your **Bio**.

{{ proof_formats() }}