+++
title = "StackExchange"

[extra]
claim_syntax = "https://stackoverflow.com/users/USERID/USERNAME"
claim_variables = ["USERID", "USERNAME"]
+++

Log in to any of the [StackExchange websites](https://stackexchange.com/sites), go to **Edit profile** and add the proof to your **About me** section.

{{ proof_formats() }}

When done, copy the link to your profile page.