+++
title = "Lichess"

[extra]
claim_syntax = "https://lichess.org/@/USERNAME"
claim_variables = ["POST_TITLE"]
+++

Log in to [lichess.org](https://lichess.org) and add the proof as a link.

{{ proof_formats(no_uri=true, no_message=true) }}