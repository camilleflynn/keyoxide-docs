+++
title = "Discourse"

[extra]
claim_syntax = "PROFILE_URL"
claim_variables = ["PROFILE_URL"]
+++

Log in to the Discourse instance's and add the proof to your **About me**.

{{ proof_formats() }}

After posting, copy the link to your profile page, it should end with your **/u/USERNAME**: this will be your **PROFILE_URL**.