+++
title = "XMPP+OMEMO"

[extra]
claim_syntax = "xmpp:XMPP_ID?omemo-sid-123456789=A1B2C3D4E5F6G7H8I9"
claim_variables = ["XMPP_ID"]
+++

Go to [mov.im](https://mov.im) and log in using your XMPP credentials. Click on **Configuration** and append the following message to the **About Me** section:

```
[Verifying my cryptographic key: FINGERPRINT_URI]
```

Using native XMPP clients that support editing the vCard data (such as [Gajim](https://gajim.org/)) should work as well. Unfortunately, this method appears unreliable and does not work for some.

Log in to the [Conversations Android app](https://conversations.im/) (or any other XMPP app that supports sharing an XMPP URI) in using your XMPP credentials. Go to **Manage accounts > Share > Share as XMPP URI**. The resulting URI should look something like:

```
xmpp:XMPP_ID?omemo-sid-123456789=A1B2C3D4E5F6G7H8I9
```

This is what you will use as **claim** in the next section.

<p class="warning">If you have followed these steps and the identity proof still doesn't work, it could be that your XMPP instance is running <a href="https://prosody.im">Prosody</a> and configured to <a href="https://prosody.im/doc/modules/mod_vcard_legacy">restrict vCard access</a>. Contact your XMPP administrator to ask if this is the case and whether it could be changed.</p>