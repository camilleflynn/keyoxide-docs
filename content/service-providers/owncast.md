+++
title = "Owncast"

[extra]
claim_syntax = "https://DOMAIN"
claim_variables = ["DOMAIN"]
+++

On the admin page of your Owncast instance, add the following lines to the `instanceDetails > socialHandles` entry in the `config.yaml`:

```
    - platform: keyoxide
      url: https://keyoxide.org/FINGERPRINT
```

When identifying your Keyoxide profile by an email address, add the following lines instead:

```
    - platform: keyoxide
      url: https://keyoxide.org/EMAIL#FINGERPRINT
```

If you use a different Keyoxide instance, replace `keyoxide.org` with the instance's domain.