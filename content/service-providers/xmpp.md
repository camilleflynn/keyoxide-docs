+++
title = "XMPP"

[extra]
claim_syntax = "xmpp:XMPP_ID"
claim_variables = ["XMPP_ID"]
+++

Go to [mov.im](https://mov.im) and log in using your XMPP credentials. Click on **Configuration** and append the proof to the **About Me** section.

{{ proof_formats() }}

Alternatively, use native XMPP clients that support editing the vCard data such as [Gajim](https://gajim.org/).

<p class="warning">If you have followed these steps and the identity proof still doesn't work, it could be that your XMPP instance is running <a href="https://prosody.im">Prosody</a> and configured to <a href="https://prosody.im/doc/modules/mod_vcard_legacy">restrict vCard access</a>. Contact your XMPP administrator to ask if this is the case and whether it could be changed.</p>