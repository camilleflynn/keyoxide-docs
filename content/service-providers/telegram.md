+++
title = "Telegram"

[extra]
claim_syntax = "https://t.me/USERNAME?proof=GROUPNAME"
claim_variables = ["USERNAME", "GROUPNAME"]
+++

Open the Telegram website or app and log in.

Create a group using **New group**.

Telegram requires you to add at least one member to create a group. Add someone you trust or just the [@KeyoxideBot](https://t.me/KeyoxideBot). This bot does not post messages and only reads the group's Description field to verify your identity claim. Note that [@KeyoxideBot](https://t.me/KeyoxideBot) does not need to be a member of your group for the claim verification to work.

Give it a descriptive title like (make sure to replace USERNAME):

```
USERNAME's Keyoxide Proof
```

In your new group, click on the top bar, open the **Edit** menu by clicking on the pencil and set the description to your proof.

{{ proof_formats() }}

Set the **Group Type** to **Public**. Change the **Public link** to whatever you like (remember what you add after the `https://t.me/...`, this will be the `GROUPNAME` you need in the claim). Under **Who can send messages?**, turn on **Only members** and **Approve new members** to avoid others joining this room. Confirm the changes to the Group Settings.

Go to **Permissions** and turn off everything for a _read only_ public room. As the group owner, you will keep all permissions.

<p class="info">It can take up to a few minutes after changing the description before the proof works.</p>